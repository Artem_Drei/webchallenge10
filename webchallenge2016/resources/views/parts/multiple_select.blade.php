<select multiple class="{{ $class }}" name="{{ $name }}">
    @foreach($options as $value => $name)
        <option value="{{ $value }}" @if(in_array($value, $activeOptions)) selected @endif>
            {{ $name }}
        </option>
    @endforeach
</select>