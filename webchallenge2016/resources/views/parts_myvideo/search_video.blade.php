@foreach ($videos as $video)
    <div class="video__item">
        <img src="{{ $video['videoInfo']->snippet->thumbnails->default->url }}" />
        <p>
            {{ $video['videoInfo']->snippet->title }}
        </p>

        <form action="{{ route('addmyvideo') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="player" value="{{ $video['videoInfo']->player->embedHtml }}" />
            <input type="hidden" name="video_id" value="{{ $video['videoId'] }}" />
            <input type="hidden" name="etag" value="{{ $video['videoInfo']->etag }}" />
            <input type="hidden" name="title" value="{{ $video['videoInfo']->snippet->title }}" />
            <input type="hidden" name="default_img" value="{{ $video['videoInfo']->snippet->thumbnails->default->url }}" />
            @if(!in_array($video['videoId'], $videoIds))
                <button class="btn btn-info-clear js-addToMyVideo" type="submit">Add to my video</button>
            @endif
        </form>

    </div>
@endforeach