<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MassVideoMsgs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mass_video_msg';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'mass_video_id', 'msg'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Get MassVideo
     * @return App\Models\MassVideo
     */
    public function massVideo()
    {
        return $this->belongsTo('App\Models\MassVideo', 'mass_video_id');
    }
}
