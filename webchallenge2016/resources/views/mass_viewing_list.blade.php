<!-- resources/views/profile.blade.php -->

@extends('base/page_cabinet', ['user' => $user])

@section ('title')
    Mass Viewing
@endsection

@section ('content_cabinet')
    <main class="main">
        <h2 class="title">Mass Video List</h2>
        <ul>
            @foreach ($massViewings as $massViewing)
                <li>
                    <iframe id="player" width="500" height="320" src="{{ $massViewing->player }}?enablejsapi=1"
                            frameborder="0"
                            allowfullscreen>
                    </iframe>
                    <a href="{{ url('profile/mass_viewing/' . urlencode($massViewing->link)) }}">
                        {{ url('profile/mass_viewing/' . urlencode($massViewing->link)) }}
                    </a>
                </li>
            @endforeach
        </ul>
    </main>
@endsection
