<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'player', 'video_id', 'etag', 'like_count', 'dislike_count', 'comment_count', 'title', 'default_img'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Get post comments.
     * @return App\Models\PostComment
     */
    public function comments()
    {
        return $this->hasMany('App\Models\PostComment', 'post_id');
    }
}
