<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Post;
use Auth;
use Youtube;
use Validator;
use Redirect;
use View;

class MyVideoController extends BaseCabinetController
{
    const COUNT_VIDEOS_ONE_PAGE = 4;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $myVideos = $this->user->posts()->paginate(self::COUNT_VIDEOS_ONE_PAGE);
        $template = 'myvideo';

        if($request->ajax()){
            $template = 'parts_myvideo/my_video';
        }


        return view($template, ['myVideos' => $myVideos]);
    }

    /**
     * @param Request $request
     */
    public function search(Request $request) {
        $q = $request->input('q', null);
        $nextPageToken = null;
        $videos = [];
        $myVideos = $this->user->posts()->getResults();
        $videoIds = get_array_vals_by_second_key($myVideos, 'video_id');

        if($q !== null) {
            $pageToken = $request->input('pageToken') ? $request->input('pageToken') : null;
            $searchVideo = $this->getYouTubeVideos($q, $pageToken);

            $videos = $this->generateVideos($searchVideo['results']);

            if (isset($searchVideo['info']['nextPageToken'])) {
                $nextPageToken = $searchVideo['info']['nextPageToken'];
            }
        }

        return [
            'q' => $q,
            'nextPageToken' => $nextPageToken,
            'videosSearchHtml' => View::make('parts_myvideo/search_video', [
                'videos' => $videos,
                'videoIds' => $videoIds
            ])->render()
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'player' => 'required',
            'video_id' => 'required|unique:posts',
            'etag' => 'required',
        ]);
        $this->response['msg'] = 'Saved.';

        if ($validator->fails()) {
            $this->response['msg'] = 'Validate error';
            $this->response['status'] = 'error';
        } else {
            $shareVideo = new Post;
            preg_match('/src\s*=\s*"(.+?)"/', $request->input('player'), $srcVideo);
            $shareVideo->player = $srcVideo[1];
            $shareVideo->video_id = $request->input('video_id');
            $shareVideo->user_id = Auth::user()->id;
            $shareVideo->etag = $request->input('etag');
            $shareVideo->title = $request->input('title');
            $shareVideo->default_img = $request->input('default_img');

            if(!$shareVideo->save()) {
                $this->response['msg'] = 'Error database';
                $this->response['status'] = 'error';
            }
        }

        return $this->response;
    }


    /**
     * Delete video.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $postId = $request->input('id');
        $post = Post::find($postId);
        $this->response['msg'] = 'Delete success.';

        if(!$post->delete()) $msg = 'Error';

        return $this->response;
    }

    /**
     * Search Video info.
     * @param  PHP objects $searchResults
     * @return array
     */
    private function generateVideos($searchResults) {
        $videos = array();
        $searchResults = empty($searchResults) ? array() : $searchResults;

        foreach ($searchResults as $result) {
            $video = array();
            $video['videoId'] = $result->id->videoId;
            $video['videoInfo'] = Youtube::getVideoInfo($video['videoId']);

            $videos[] = $video;
        }

        return $videos;
    }

    /**
     * Search youtube video.
     * @param  string $q
     * @param  string $pageToken
     * @return array
     */
    private function getYouTubeVideos($q = '', $pageToken = null)
    {
        $params = array(
            'q' => $q,
            'type' => 'video',
            'part' => 'id',
            'maxResults' => 8,
            'pageToken' => $pageToken
        );

        return $search = Youtube::searchAdvanced($params, true);
    }
}
