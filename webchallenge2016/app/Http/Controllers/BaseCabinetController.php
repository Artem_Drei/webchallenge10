<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Auth;

class BaseCabinetController extends Controller
{
    protected $user;

    protected $leftMenuItems = [
        'profile/my_video' => [
            'name' => 'MyVideo',
            'iconAdditionalClass' => 'fa-youtube'
        ],
        'profile/friends' => [
            'name' => 'Friends',
            'iconAdditionalClass' => 'fa-users'
        ],
        'profile/mass_viewing' => [
            'name' => 'MassVideo',
            'iconAdditionalClass' => 'fa-share-alt'
        ],
        'profile/inbox' => [
            'name' => 'Inbox',
            'iconAdditionalClass' => 'fa-share-alt'
        ]
    ];

    protected $response = [
        'status' => 'success',
        'msg' => ''
    ];

    public function __construct()
    {
        $this->user = Auth::user();

        View::share('user', $this->user);
        View::share('leftMenuItems', $this->leftMenuItems);
    }
}
