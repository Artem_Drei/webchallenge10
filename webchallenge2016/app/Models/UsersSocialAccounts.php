<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersSocialAccounts extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_social_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['social_type', 'user_id', 'token', 'social_id', 'name', 'nickname', 'email', 'avatar'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
