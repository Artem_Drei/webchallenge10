<!-- resources/views/profile.blade.php -->

@extends('base/page_cabinet', ['user' => $user])

@section ('title')
    My video
@endsection

@section ('content_cabinet')
    <main class="main my-videos">
        <h2 class="title">Videos</h2>

        <form action="{{ route('searchvideo') }}" method="POST" class="js-searchVideo">
            {{ csrf_field() }}
            <input type="text" name="q" placeholder="Search" class="js-query" />
            <input type="hidden" name="pageToken" value="" class="js-pageToken" />
            <button class="btn btn-info"  type="submit"> <i class="fa fa-search" aria-hidden="true"></i> Search </button>
            <button class="btn btn-primary js-showNext" type="submit" style="display:none">Show Next <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
        </form>

        <div class="video video-search js-videoSearchBlock"></div>

        <!-- @include('parts_myvideo/my_video', ['myVideos' => $myVideos])
        @include('parts/loader') -->

        <div class="step">

          <ul class="steps">
            <li class="step step--incomplete step--active">
              <span class="step__icon"></span>
              <span class="step__label">Step 1</span>
            </li>
            <li class="step step--incomplete step--inactive">
              <span class="step__icon"></span>
              <span class="step__label">Step 2</span>
            </li>
            <li class="step step--incomplete step--inactive">
              <span class="step__icon"></span>
              <span class="step__label">Step 3</span>
            </li>
          </ul>

          <div class="step__item step_one animated fadeIn">
            <h2>Plese, Search a video and add it your videos.</h2>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
          <div class="step__item step_two animated">
            <h2>Plese, add your friends for a translation.</h2>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
          <div class="step__item step_tree animated">
            <h2>Shere this link with your friend</h2>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </div>
        </div>
    </main>
@endsection

@section('js')
    @parent
    <script src="{{ asset('js/Clasess/MyVideo.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/my_video.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/libraries/infinity_scroll/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/libraries/infinity_scroll/infinite.min.js') }}" type="text/javascript"></script>
@endsection
