<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Post;
use App\Models\MassVideo;
use App\Models\MassVideoUser;
use Auth;
use Validator;
use Redirect;

class MassVideoController extends BaseCabinetController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $massViewings = MassVideo::where('user_id', $this->user->id)->get();

        return view('mass_viewing_list', ['massViewings' => $massViewings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $postId = $request->input('id');
        $post = Post::find($postId);

        $massViewing = new MassVideo;
        $massViewing->player = $post->player;
        $massViewing->user_id = $this->user->id;
        $massViewing->link = md5($post->player . time());
        $msg = 'Error.';

        if($massViewing->save()) {
            $msg = 'Succes.';

            return Redirect::to('profile/mass_viewing/' . $massViewing->link)->withErrors($msg);
        } else {
            return Redirect::back()->withErrors($msg);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hash)
    {
        $massViewing = MassVideo::where('link', $hash)->first();

        if(MassVideoUser::validateMassViewing($massViewing, $this->user)) {
            $massVideoUsers = MassVideoUser::where('mass_video_id', $massViewing->id)->get();
            $videoUsers = [];

            foreach ($massVideoUsers as $massVideoUser) {
                $videoUsers[] = $massVideoUser->user()->first();
            }

            $friendsOptions = prepare_entities_for_select_options($this->user->friends());

            return view('mass_viewing', ['massViewing' => $massViewing,
                                         'videoUsers' => $videoUsers,
                                         'friendsOptions' => $friendsOptions
            ]);
        }

        return Redirect::back()->withErrors('Not access permission');
    }

    /**
     * Add user for massViewing.
     *
     * @return \Illuminate\Http\Response
     */
    public function addUser(Request $request)
    {
        $friendIds = $request->input('friends');
        $massVideoId = $request->input('mass_video_id');

        if(!is_array($friendIds)) {
            $friendIds = [0 => $friendIds];
        }

        foreach ($friendIds as $friendId) {
            MassVideoUser::create([
                'user_id' => $friendId,
                'mass_video_id' => $massVideoId
            ]);
        }

        return Redirect::back();
    }
}
