<!-- resources/views/profile.blade.php -->

@extends('base/page_cabinet', ['user' => $user])

@section ('title')
    Friends
@endsection

@section ('style')
    @parent
    <link href="{{ asset('css/libraries/jquery-ui.min.css') }}" rel="stylesheet">
    <style>
        .ui-menu li div span {
            font-weight: bold;
        }
    </style>
@endsection

@section('js')
    @parent
    <script src="{{ asset('js/libraries/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/Clasess/Friends.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/friends.js') }}" type="text/javascript"></script>
@endsection

@section ('content_cabinet')
    <main class="main friends">
        <h2 class="title">Friends</h2>

        <h3 class="invite-friend__title">Pleace, enter users email and invite him to your friens list.</h3>
        <div class="invite-friend__search-wrapper">
            <form action="{{ route('searchfriends') }}" class="js-searchForm" method="POST">
                {{ csrf_field() }}
                <input class="invite-friend__input js-searchFriends" type="text" placeholder="friend@email.com" name="q">
            </form>
            <form action="{{ route('invitefriends') }}" class="js-inviteForm" method="POST">
                {{ csrf_field() }}
                <input type="hidden" class="js-userEmail" name="user_email" value="">
                <button class="invite-friend__btn">Invite</button>
            </form>
        </div>

        <div class="friend__table js-friends">
            @include ('parts_friends/friend_list', ['friends' => $friends])
        </div>
    </main>
@endsection
