<?php

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BaseSocket;
use Ratchet\ConnectionInterface;
use App\Models\MassVideo;

class MassViewingSocket extends BaseSocket
{
    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);

        echo "New connection!\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $fromHash = $this->getQueryParamConn($from, 'hash');
        $fromUserId = $this->getQueryParamConn($from, 'userId');
        $data = json_decode($msg);

        if($data->type == 'video' && !MassVideo::isCreatorMassVideo($fromHash, $fromUserId)) return false;

        foreach ($this->clients as $client) {
            $clientHash = $this->getQueryParamConn($client, 'hash');

            if ($from !== $client && $clientHash == $fromHash) {
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);

        echo "Connection ($conn->resourceId) has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    private function getQueryParamConn(ConnectionInterface $conn, $param) {
        $query = $conn->WebSocket->request->getQuery()->toArray();

        return isset($query[$param]) ? $query[$param] : '';
    }
}