function MassViewing(){
    this.conn;
    this.webSocketUrl;
    this.webScoketPort = 8080;
    this.transferProtocol = 'http';
    this.webSocketHashPage = '';
    this.webSocketUserId = '';
    this.funcAfterOnMsg = null;

    this.player = null;
    this.playerId = 'player';

    this.init = function () {
        var self = this;
        this.conn = new WebSocket(this.getWebSocketQueryStr());

        this.conn.onopen = function(e) {
            console.log("Connection established");
        };

        this.conn.onmessage = function(e) {
            var data = JSON.parse(e.data);
            console.log('Полученные данные: ', data);

            if(data.type == 'chat') {
                if(typeof self.funcAfterOnMsg === 'function') self.funcAfterOnMsg(data.msg);
            } else if(data.type == 'video') {

                switch (data.action) {
                    case YT.PlayerState.PAUSED:
                        self.player.pauseVideo();
                        break;

                    case YT.PlayerState.PLAYING:
                        self.player.playVideo();
                        break;

                    default:
                        console.info('another action')
                }

                self.player.seekTo(data.currentTime, true);
            }
        };

        this.loadIframeApi();
    };

    this.loadIframeApi = function() {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    };

    this.iniitPlayer = function() {
        this.player = new YT.Player('player', {
            events: {
                'onStateChange': 'onPlayerStateChange'
            }
        });
    };

    this.send = function (msg) {
        this.conn.send(JSON.stringify(msg));
        console.log('Отправлено: ', msg);
    };

    this.getWebSocketQueryStr = function () {
        var webSocketQueryStr = '';

        if(this.getTransferProtocol() == 'http') webSocketQueryStr += 'ws://';
        if(this.getTransferProtocol() == 'https') webSocketQueryStr += 'wss://';

        webSocketQueryStr += this.getWebSocketUrl();
        webSocketQueryStr += ':' + this.getWebScoketPort();

        if(this.getWebSocketHashPage()) {
            webSocketQueryStr += '?hash=' + this.getWebSocketHashPage();
        }

        if(this.getWebSocketUserId()) {
            webSocketQueryStr += '&userId=' + this.getWebSocketUserId();
        }

        return webSocketQueryStr;
    };

    this.setWebSocketUrl = function(url) {
        this.webSocketUrl = url;
    };

    this.getWebSocketUrl = function() {
        return this.webSocketUrl;
    };

    this.setWebScoketPort = function(port) {
        this.webScoketPort = port;
    };

    this.getWebScoketPort = function() {
        return this.webScoketPort;
    };

    this.setTransferProtocol = function(transferProtocol) {
        this.transferProtocol = transferProtocol;
    };

    this.getTransferProtocol = function() {
        return this.transferProtocol;
    };

    this.setWebSocketHashPage = function(WebSocketHashPage){
        this.webSocketHashPage = WebSocketHashPage;
    };

    this.getWebSocketHashPage = function(){
        return this.webSocketHashPage;
    };

    this.setWebSocketUserId = function(WebSocketUserId){
        this.webSocketUserId = WebSocketUserId;
    };

    this.getWebSocketUserId = function(){
        return this.webSocketUserId;
    };

    this.setFuncAfterOnMsg = function(func){
        this.funcAfterOnMsg = typeof func === 'function' ? func : null;
    };
};

var massViewing = new MassViewing();