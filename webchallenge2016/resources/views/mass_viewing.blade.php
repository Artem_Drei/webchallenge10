<!-- resources/views/profile.blade.php -->

@extends('base/page_cabinet', ['user' => $user])

@section ('title')
    Mass Viewing
@endsection

@section ('content_cabinet')
        <input type="hidden" class="js-userId" value="{{ $user->id  }}" />
        <main class="main">
            <h2 class="title">Share video with freinds</h2>
            <div class="share">
                <div class="share__link">
                    <i class="js-add-friend fa fa-share-alt-square"></i>
                    <input type="text" value="{{ url('profile/mass_viewing/' . urlencode($massViewing->link)) }}">
                </div>
                <div class="share__wrapper">
                    <div class="friend">
                        <div class="friend__table">
                            <form action="{{ url('profile/mass_viewing/add_user') }}" method="POST">
                                <input type="hidden" name="mass_video_id" value="{{ $massViewing->id }}" />
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                @include('parts/multiple_select', [
                                    'options' => $friendsOptions,
                                    'class' => '',
                                    'activeOptions' => [],
                                    'name' => 'friends'
                                ])

                                <button class="invite-friend__btn">Submit</button>
                            </form>

                            <div>
                                @foreach($videoUsers as $user)
                                    <div class="friend__item" href="#">
                                        <img class="friend__avatar" src="{{ $user->avatar }}" alt="User Avatar">
                                        <span class="friend__user-name">{{ $user->name }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="video__item">
                        <iframe id="player" width="500" height="320" src="{{ $massViewing->player }}?enablejsapi=1"
                                frameborder="0"
                                allowfullscreen>
                        </iframe>
                    </div>

                    <div class="chat">
                        <div class="chat-title">
                            <h1>{{ $user->name }}</h1>
                            <figure class="avatar">
                                <img src="{{ $user->avatar }}" />
                            </figure>
                        </div>
                        <div class="messages">
                            <div class="messages-content"></div>
                        </div>
                        <div class="message-box">
                            <textarea type="text" class="message-input" placeholder="Type message..."></textarea>
                            <button type="submit" class="message-submit">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        
        <span class="js-link" data-link={{ $massViewing->link }}></span>
@endsection

@section('js')
    @parent
    <script src="{{ asset('js/Clasess/MassViewing.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/mass_viewing.js') }}" type="text/javascript"></script>
@endsection
