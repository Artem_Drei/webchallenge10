<!-- resources/views/base/page.blade.php -->
@extends('base/page')

@section ('js')
    <script src="{{ asset('js/libraries/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('js/libraries/alertify.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection
@section ('content')
    <body class="body">
        <div class="container cabinet">

            @include('parts/header', ['user' => $user])

            <!-- <div class="content">   -->
            
                @yield('content_cabinet')
            <!-- </div> -->
        </div>
    </body>
@endsection
