massViewing.setWebSocketUrl('webchallenge2016.com');
massViewing.setWebSocketHashPage($('.js-link').data("link"));
massViewing.setWebSocketUserId($('.js-userId').val());
massViewing.setFuncAfterOnMsg(updateHtmlChat);
massViewing.init();

function onYouTubeIframeAPIReady() {
    massViewing.iniitPlayer();
}

var $messages = $('.messages-content');

$(window).load(function() {
  $messages.mCustomScrollbar();
});

function updateScrollbar() {
  $messages.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
    scrollInertia: 10,
    timeout: 0
  });
};

$(window).on('keydown', function(e) {
    if (e.which == 13) {
        insertMessage();
        return false;
    }
});

function insertMessage() {
    msg = $('.message-input').val();
    if ($.trim(msg) == '') {
        return false;
    }

    updateHtmlChat(msg);

    var data = {
      type: 'chat',
      msg: msg
    };

    massViewing.send(data);
}

function onPlayerStateChange(event) {
    var data = {
        type: 'video'
    };

    switch (event.data) {
        case YT.PlayerState.PAUSED:
            data.action = YT.PlayerState.PAUSED;
            break;

        case YT.PlayerState.PLAYING:
            data.action = YT.PlayerState.PLAYING;
            break;

        default:
            console.info('another action')
    }

    if (typeof data.action != undefined) {
        data.currentTime = massViewing.player.getCurrentTime();
        massViewing.send(data);
    }
};

function updateHtmlChat(msg){
    console.info('updateHtmlChat', msg);
    $('<div class="message message-personal">' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
    $('.message-input').val(null);
    updateScrollbar();
}

$('.message-submit').click(function() {
    insertMessage();
});

