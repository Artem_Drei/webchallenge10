<!-- resources/views/main.blade.php -->

@extends('base/page')

@section ('title')
    Login With Social
@endsection

@section ('content')
    <body class="login-body">
        <div class="container login">
            <div class="login__container">
                <i class="login__main-icon fa fa-youtube"></i>
                <div class="login__description-wrapper">
                    <h3>YouTube Share</h3>
                    <ul class="description">
                        <li class="description__item"><i class="fa fa-user-plus"></i>Share Your Video with friends</li>
                        <li class="description__item"><i class=" fa fa-youtube-play"></i>Controll playing for everyone</li>
                        <li class="description__item"><i class="fa fa-wechat"></i>Chat with no problems</li>
                        <li class="description__item"><i class="fa fa-video-camera"></i>Create You Video Library</li>
                    </ul>
                </div>
                <div class="login__form-wrapper">
                    <form class="login__form">
                        <h3>Log in with your account</h3>
                        <a href="{{{ route('socialAuth', array('provider' => 'facebook')) }}}" class="btn btn__facebook"><i class="fa fa-facebook pull-left"></i>Facebook</a>
                        <a href="{{{ route('socialAuth', array('provider' => 'google')) }}}" class="btn btn__google"><i class="fa fa-google-plus pull-left"></i>Google+ </a>
                    </form>
                </div>
            </div>
        </div>
    </body>
@endsection
