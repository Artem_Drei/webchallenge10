function MyVideo() {
    this.searchVideo = function(form) {
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            datatype: 'json',
            success: function(data){
                $('.js-videoSearchBlock').html(data.videosSearchHtml);
                $('.js-pageToken').val(data.nextPageToken);
                $('.js-query').val(data.q);

                if(data.nextPageToken) {
                    $('.js-showNext').show();
                } else {
                    $('.js-showNext').hide();
                }
                $('.loader-wrapper').hide();

            },
            error: function(error) {
                console.error(error);
            },
            beforeSend: function() {
                $('.loader-wrapper').show();
            }
        });
    };

    this.add = function(form, submitButton) {
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            datatype: 'json',
            success: function(data){
                alertify.log(data.msg, data.status);
                submitButton.remove();
            },
            error: function(error) {
                console.error(error);
            }
        });
    };

    this.delete = function(form) {
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            datatype: 'json',
            success: function(data){
                alertify.log(data.msg, data.status);
                form.closest('li').remove();
            },
            error: function(error) {
                console.error(error);
            }
        });
    }
};

var myVideo = new MyVideo();