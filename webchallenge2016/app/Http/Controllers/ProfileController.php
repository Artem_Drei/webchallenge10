<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Post;
use App\Models\MassVideo;
use App\Models\MassVideoUser;
use App\Models\UsersSocialAccounts;
use Illuminate\Support\Facades\View;
use Auth;
use Youtube;
use Validator;
use Redirect;

class ProfileController extends BaseCabinetController
{
    /**
     * Display a listing of the resource.
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('profile');
    }
}
