<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\UserInvite;
use App\Models\UserInbox;
use App\Models\User;
use App\Models\UserFriends;
use Redirect;

class UserInviteController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, $hash, $status)
    {
        $userInvite = UserInvite::where('hash', $hash)->get()->first();
        
        if(!UserFriends::isExistFriend($userInvite->user_id, $userInvite->user_invite_id) && !$userInvite->status) {
            $userInvite->status = $status;

            if($userInvite->save()) {
                $user  = User::find($userInvite->user_id);

                if($status == UserInvite::STATUS_CONFIRM) {
                    UserFriends::create([
                        'user_id' => $userInvite->user_id,
                        'friend_id' => $userInvite->user_invite_id
                    ]);

                    UserInbox::create([
                        'user_id' => $userInvite->user_id,
                        'msg' => $user->name . ', confirm you invite'
                    ]);
                } elseif($status == UserInvite::STATUS_DECLINE) {
                    UserInbox::create([
                        'user_id' => $userInvite->user_id,
                        'msg' => $user->name . ', decline you invite'
                    ]);
                }
            }
        }

        return Redirect::back();
    }

}
