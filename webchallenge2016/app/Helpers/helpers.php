<?php

if (!function_exists('prepare_entities_for_select_options')) {
    function prepare_entities_for_select_options($entities, $key = 'id', $value = "name") {
        $result = [];

        foreach ($entities as $entity) {
            if(isset($entity[$key]) && isset($entity[$value]))  {
                $result[$entity[$key]] = $entity[$value];
            }
        }

        return $result;
    }
}

if (!function_exists('get_array_vals_by_second_key')) {
    function get_array_vals_by_second_key($array, $secondKey, $thirdKey = null) {
        $result = array();
        foreach ($array as $val) {
            if ($thirdKey) {
                if (isset($val[$secondKey][$thirdKey])) {
                    $result[] = $val[$secondKey][$thirdKey];
                }
            } else {
                if (isset($val[$secondKey])) {
                    $result[] = $val[$secondKey];
                }
            }
        }
        return $result;
    }
}

if (!function_exists('get_array_vals_by_second_key_concat_third_key')) {
    function get_array_vals_by_second_key_concat_third_key($array, $secondKey, $thirdKey, $delimiter = ' - ') {
        $result = array();
        foreach ($array as $val) {
            if (isset($val[$secondKey]) && isset($val[$thirdKey])) {
                $str = $val[$secondKey];

                if(!empty($val[$thirdKey])) $str .= $delimiter . $val[$thirdKey];

                $result[] = $str;
            }
        }
        return $result;
    }
}