<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFriends extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_friends';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'friend_id'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Get Friend
     * @return App\Models\User
     */
    public function userBYFriendId()
    {
        return $this->belongsTo('App\Models\User', 'friend_id');
    }

    public static function isExistFriend($userId, $friendId) {
        return UserFriends::where('friend_id', $friendId)->where('user_id', $userId)->count();
    }
}
