$(document).ready(function(){
    $(document).on('submit', '.js-searchVideo', function(event) {
        myVideo.searchVideo($(this));

        event.preventDefault();
        return false;
    });

    $(document).on('click', '.js-addToMyVideo', function() {
        var form = $(this).closest('form');
        myVideo.add(form, $(this));

        event.preventDefault();
        return false;
    });

    $(document).on('submit', '.js-deleteMyVideo', function() {
        myVideo.delete($(this));

        event.preventDefault();
        return false;
    });

    var infinite = new Waypoint.Infinite({
        element: $('.infinite-container')[0],
        onBeforePageLoad: function() {
            $('.loader-wrapper').show();
        },
        onAfterPageLoad : function(items) {
            $('.loader-wrapper').hide();
        }
    });


    $('.steps').on('click', '.step--active', function() {
      $(this).removeClass('step--incomplete').addClass('step--complete');
      $(this).removeClass('step--active').addClass('step--inactive');
      $(this).next().removeClass('step--inactive').addClass('step--active');
    });

    $('.steps').on('click', '.step--complete', function() {
      $(this).removeClass('step--complete').addClass('step--incomplete');
      $(this).removeClass('step--inactive').addClass('step--active');
      $(this).nextAll().removeClass('step--complete').addClass('step--incomplete');
      $(this).nextAll().removeClass('step--active').addClass('step--inactive');
    });

});
