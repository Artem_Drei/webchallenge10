<!-- resources/views/main_menu.blade.php -->

@foreach ($leftMenuItems as $url => $item)
    <li class="menu__item">
        <i class="fa {{ $item['iconAdditionalClass']  }}"></i>
        <a href="{{ url($url) }}">{{ $item['name']  }}</a>
    </li>
@endforeach