<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '449919225209554', // App ID
        'client_secret' => '2e6e7da7a43731f55490d8e744e57e2f',//'2e6e7da7a43731f55490d8e744e57e2f', // App Secret
        'redirect' => 'http://'. env('DOMAIN').'/auth/facebook/callback', 
    ],

    'google' => [
        'client_id' => '676907866831-badta7ufgdu3obe4jqifj9lbemlinvft.apps.googleusercontent.com',
        'client_secret' => 'mwMX-yQ1RSX4IcWv9XlsQH4u',
        'redirect' => 'http://'. env('DOMAIN').'/auth/google/callback',
    ],

];
