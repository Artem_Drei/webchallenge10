function Friends() {
    this.searchFormClass = '.js-searchForm';

    this.search = function(q, callback) {
        var form = $(this.searchFormClass);

        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            datatype: 'json',
            success: function(data){
                console.log(data);
                callback(data);
            },
            error: function(error) {
                console.error(error);
            }
        });
    };

    this.invite = function(form) {
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            datatype: 'json',
            success: function(data){
                console.log(data);
                if(data.status == 'success') {
                    $('.js-searchFriends').val('');
                    $('.js-friends').html(data.friends);
                }

                alertify.log(data.msg, data.status);
            },
            error: function(error) {
                console.error(error);
            }
        });
    };

    this.delete = function(form, friendBlock) {
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            datatype: 'json',
            success: function(data){
                console.log(data);
                friendBlock.remove();
                alertify.log(data.msg, data.status);
            },
            error: function(error) {
                console.error(error);
            }
        });
    };

    this.strip = function strip(html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent||tmp.innerText;
    };
};

var friendsObj = new Friends();