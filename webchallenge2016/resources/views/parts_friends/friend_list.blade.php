@foreach ($friends as $friend)
    <div class="friend__item js-friendItem">
        <img class="friend__avatar" src="{{ $friend->avatar }}" alt="User Avatar">
        <span class="friend__user-name">{{ $friend->name }}</span>
        <form method="POST" action="{{ route('frienddelete', ['id' => $friend->id]) }}" class="js-deleteFriend">
            {{ csrf_field() }}
            <button class="btn delete-friend">X</button>
        </form>

    </div>
@endforeach