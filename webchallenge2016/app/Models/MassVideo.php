<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MassVideo extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mass_video';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'player', 'link'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * isCreator Mass Viewing.
     * @param  $hash
     * @param  $userId
     */
    public static function isCreatorMassVideo($hash, $userID) {
        $massViewing = self::where('link', $hash)->first();
        if($massViewing) {
            if($massViewing->user_id == $userID) return true;
        }

        return false;
    }
}
