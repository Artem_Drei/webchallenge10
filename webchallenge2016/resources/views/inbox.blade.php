<!-- resources/views/profile.blade.php -->

@extends('base/page_cabinet', ['user' => $user])

@section ('title')
    Inbox
@endsection

@section ('style')
    @parent
@endsection

@section('js')
    @parent
@endsection

@section ('content_cabinet')
    @foreach ($msgs as $msg)
        <div class="">
            {!! $msg->msg !!}
        </div>
    @endforeach
@endsection
