<?php

namespace App\Http\Controllers;

use App\Models\UserFriends;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\UserInvite;
use App\Models\UserInbox;
use App\Models\Post;
use App\Models\MassVideo;
use App\Models\MassVideoUser;
use App\Models\UsersSocialAccounts;
use Illuminate\Support\Facades\View;
use Auth;
use Youtube;
use Validator;
use Redirect;

class FriendsController extends BaseCabinetController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $friends = $this->user->friends()->getResults();

        return view('friends', ['friends' => $friends]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->response['msg'] = 'Invite send success';

        $validator = Validator::make($request->all(), [
            'user_email' => ['required', 'email']
        ]);

        if ($validator->fails()) {
            $this->response['msg'] = 'Validate error';
            $this->response['status'] = 'error';
        } else {
            $friend = User::where('email', '=', $request->input('user_email'))->first();

            if($friend) {

                if(UserFriends::isExistFriend($this->user->id, $friend->id)) {
                    $this->response['msg'] = 'There is such a friend';
                    $this->response['status'] = 'error';
                } else {
                    $hash = md5($friend->id . $this->user->id . time());
                    UserInvite::create([
                        'user_invite_id' => $friend->id,
                        'user_id' => $this->user->id,
                        'hash' => $hash
                    ]);

                    UserInbox::create([
                        'user_id' => $friend->id,
                        'msg' => 'I received an invitation from ' . $this->user->name .
                            '<a href="user_invite/update_status/' . $hash . '/confirm">confirm</a>
                            <a href="user_invite/update_status/' . $hash . '/decline">decline</a>'
                    ]);

                    $this->response['friends'] = View::make('parts_friends/friend_list', [
                        'friends' => $this->user->friends()->getResults()
                    ])->render();
                }

            } else {
                $this->response['msg'] = 'This user does not exists';
                $this->response['status'] = 'error';
            }
        }

        return $this->response;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $q = $request->input('q', '');
        $friends = $this->user->friends()->getResults();
        $friendIds = get_array_vals_by_second_key($friends, 'id');
        $friendIds[] = $this->user->id;

        $users = User::whereNotIn('id', $friendIds)->where(function($query) use ($q) {
            $query->where('email', 'like', '%' . $q . '%')
                ->orWhere('name', 'like', '%' . $q . '%');
        })->get();

        return get_array_vals_by_second_key_concat_third_key($users, 'name', 'email');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->response['msg'] = 'Delete Success.';

        $friend = UserFriends::where([
            'friend_id' => $id,
            'user_id' => $this->user->id
        ]);

        if(!$friend->delete()) {
            $this->responce['status'] = 'error';
            $this->response['msg'] = 'Error. Friend did not deleted.';
        }

        return $this->response;
    }
}
