<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInvite extends Model
{
    protected $table = 'user_invite';
    const STATUS_CONFIRM = 'confirm';
    const STATUS_DECLINE = 'decline';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'user_invite_id', 'hash', 'status'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Get User Invite
     * @return App\Models\User
     */
    public function userInvite()
    {
        return $this->belongsTo('App\Models\User', 'user_invite_id');
    }
}
