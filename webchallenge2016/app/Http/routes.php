<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');

//auth
Route::get('logout', ['as' => 'logout',
    'uses' => 'Auth\AuthController@logout']);

//social auth
Route::get('auth/{provider}', ['as' => 'socialAuth',
    'uses' => 'Auth\AuthController@redirectToProvider']);
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

//profile

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', ['as' => 'profile',
            'uses' => 'ProfileController@index']);

        //MyVideoController
        Route::get('my_video', ['as' => 'myvideo',
            'uses' => 'MyVideoController@index']);

        Route::post('my_video/search', ['as' => 'searchvideo',
            'uses' => 'MyVideoController@search']);

        Route::post('my_video/add', ['as' => 'addmyvideo',
            'uses' => 'MyVideoController@create']);

        Route::post('my_video/delete', ['as' => 'deletemyvideo',
            'uses' => 'MyVideoController@delete']);

        //MassVideoController
        Route::get('mass_viewing', ['as' => 'massviewing',
            'uses' => 'MassVideoController@index']);

        Route::get('mass_viewing/{hash}', ['as' => 'massviewing',
            'uses' => 'MassVideoController@show']);

        Route::post('create_mass_viewing', ['as' => 'createmassviewing',
            'uses' => 'MassVideoController@create']);

        Route::post('mass_viewing/add_user', ['as' => 'massviewing',
            'uses' => 'MassVideoController@addUser']);

        //FriendsController
        Route::get('friends', ['as' => 'friends',
            'uses' => 'FriendsController@index']);

        Route::post('friends/invite', ['as' => 'invitefriends',
            'uses' => 'FriendsController@create']);

        Route::post('friends/search', ['as' => 'searchfriends',
            'uses' => 'FriendsController@search']);

        Route::post('friends/delete/{id}', ['as' => 'frienddelete',
            'uses' => 'FriendsController@destroy']);

        //UserInviteController
        Route::get('user_invite/update_status/{hash}/{status}', ['as' => 'inviteupdatestatus',
            'uses' => 'UserInviteController@updateStatus']);

        //UserInboxController
        Route::get('inbox', ['as' => 'inbox',
            'uses' => 'UserInboxController@index']);
    });

});