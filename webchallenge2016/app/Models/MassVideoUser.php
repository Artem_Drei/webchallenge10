<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MassVideoUser extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mass_video_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'mass_video_id'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Get MassVideo
     * @return App\Models\MassVideo
     */
    public function massVideo()
    {
        return $this->belongsTo('App\Models\MassVideo', 'mass_video_id');
    }


    /**
     * Validate Mass Viewing.
     * @param  App\Models\MassVideo $massViewing
     * @param  App\Models\User $user
     */
    public static function validateMassViewing($massViewing, $user) {
        if($massViewing) {
            $massViewingUser = MassVideoUser::where('user_id', $user->id)
                ->where('mass_video_id', $massViewing->id)->first();

            if($massViewingUser || ($massViewing->user_id == $user->id)) return true;
        }

        return false;
    }
}
