<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInbox extends Model
{
    protected $table = 'user_inbox';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'msg', 'status'];

    /**
     * Get User
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
