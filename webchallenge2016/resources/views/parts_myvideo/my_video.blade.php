<ul class="video video-my infinite-container">
    @foreach ($myVideos as $video)
        <li class="video__item infinite-item">
            <img src="{{ $video->default_img }}" />
            <p>
                {{ $video->title }}
            </p>

            <form class="video__delete js-deleteMyVideo" action="{{ route('deletemyvideo') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $video->id }}" />

                <button class="btn btn-danger" type="submit"> <i class="fa fa-file-video-o" aria-hidden="true"></i> Delete</button>
            </form>

            <form class="video__mass-viewing" action="{{ route('createmassviewing') }}" method="POST">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="id" value="{{ $video->id }}" />

                <button class="btn btn-primary" type="submit"> <i class="fa fa-trash-o" aria-hidden="true"></i> Mass Viewing</button>
            </form>
        </li>
    @endforeach
</ul>
@if($myVideos->nextPageUrl())
    <a class="infinite-more-link js-link-more" style="display:none" href="{{ $myVideos->nextPageUrl() }}">More</a>
@endif