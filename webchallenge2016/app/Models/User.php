<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'avatar'];

    /**
     * Get SocialAccounts.
     * @return App\Models\UsersSocialAccounts
     */
    public function social_accounts()
    {
        return $this->hasMany('App\Models\UsersSocialAccounts', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function friends()
    {
        return $this->belongsToMany('App\Models\User', 'user_friends', 'user_id', 'friend_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function theFriends()
    {
        return $this->belongsToMany('App\Models\User', 'user_friends', 'friend_id', 'user_id');
    }


    /**
     * Get SocialAccounts.
     * @return App\Models\UsersSocialAccounts
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'user_id');
    }

    /**
     * Get Mass Video.
     * @return App\Models\UsersSocialAccounts
     */
    public function massVideos()
    {
        return $this->hasMany('App\Models\MassVideo', 'user_id');
    }

    /**
     * Get SocialAccounts.
     * @return App\Models\UsersSocialAccounts
     */
    public function inboxMsg()
    {
        return $this->hasMany('App\Models\UserInbox', 'user_id');
    }
}
