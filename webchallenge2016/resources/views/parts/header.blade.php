<!-- resources/views/header.blade.php -->

<header class="header">
	<ul class="header__menu">
		@include('parts/main_menu')
		<li class="user__name">{{ $user->name }}
			<ul class="user__drop-down">
				<li>
					<a class="user__drop-down__item" href="{{ route('logout') }}">Logout</a>
				</li>
			</ul>
		</li>
		<li>
			<img class="user__avatar" src="{{ $user->avatar }}" alt="user avatar">
		</li>
	</ul>
</header>

@if($errors->any())
    <script>
		alertify.error({{$errors->first()}});
	</script>
@endif
