<!-- resources/views/base/page.blade.php -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
        <link rel="stylesheet" href="{{ asset('img/font-awesome/font-awesome.css') }}">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('css/libraries/alertify.core.css') }}" rel="stylesheet">
        <link href="{{ asset('css/libraries/alertify.default.css') }}" rel="stylesheet">

        @yield('style')

        <script src="{{ asset('js/libs.min.js') }}"></script>
        @yield('js')
    </head>

    @yield('content')

</html>