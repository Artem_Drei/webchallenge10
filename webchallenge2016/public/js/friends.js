$(document).ready(function(){
    var inputSearch = '.js-searchFriends';

    $(document).on('submit', '.js-inviteForm', function(event) {
        friendsObj.invite($(this));

        event.preventDefault();
        return false;
    });

    $(document).on('submit', '.js-deleteFriend', function(event) {
        friendsObj.delete($(this), $(this).closest('.js-friendItem'));

        event.preventDefault();
        return false;
    });

    $(inputSearch).autocomplete({
        source: function(request, callback) {
            var searchParam  = request.term;
            friendsObj.search(searchParam, callback);
        },
        appendTo: friendsObj.searchFormClass,
        select: function( event, ui ) {
            var newValues = friendsObj.strip(ui.item.value).split('-');
            newValues = typeof newValues[1] !== undefined ? newValues[1].trim() : '';
            $('.js-userEmail').val(newValues);

        },
        open: function(event, ui) {
            $('.ui-autocomplete').off('menufocus hover mouseover');
        }
    }).focus(function(){
        $(this).autocomplete("search");
    });

    var widgetInst = $(inputSearch).autocomplete('instance');

    widgetInst._renderMenu = function( ul, items ) {
        var that = this;
        $.each( items, function( index, item ) {
            that._renderItemData( ul, item );
        });

        $(ul).find('li div').each(function(index) {
            var str = $(this).text();

            var q = $(inputSearch).val();
            var words = q.split(' ');

            if(words.length > 0) {
                q = words.join('|');
            }

            var reg = new RegExp('(' + q + ')', 'gi');
            $(this).html(str.replace(reg, "<span>$1</span>"));
        });
    };
});